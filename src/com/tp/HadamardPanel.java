package com.tp;

import javax.swing.*;
import javax.swing.tree.ExpandVetoException;
import java.awt.*;

/**
 * Created by christian on 27/10/15.
 */
public class HadamardPanel extends JPanel {

    private int N = 0;

    public HadamardPanel(int n) throws Exception {
        setPreferredSize(new Dimension(480, 480));
        setVisible(true);

        if ( n < 0 ) {
            throw new Exception("El valor de N debe ser positivo");
        }

        N = (int)Math.pow(2, n);
    }

    @Override
    protected void paintComponent(Graphics go) {
        super.paintComponent(go);

        Dimension dim = getPreferredSize();

        drawHadamard((Graphics2D)go, 0, 0, (int)dim.getWidth(), (int)dim.getHeight(), N, Color.BLACK, Color.WHITE);
    }

    private void drawHadamard(Graphics2D g, int x, int y, int w, int h, int N, Color primaryColor, Color secundaryColor) {
        int halfW = w/2;
        int halfH = h/2;

        N = N/2;

        if ( N <= 1 ) {
            drawRectangle(g, x, y, halfW, halfH, primaryColor);
            drawRectangle(g, x + halfW, y, halfW, halfH, primaryColor);
            drawRectangle(g, x, y + halfH, halfW, halfH, primaryColor);
            drawRectangle(g, x + halfW, y + halfH, halfW, halfH, secundaryColor);
        } else {
            drawHadamard(g, x, y, halfW, halfH, N, primaryColor, secundaryColor);
            drawHadamard(g, x + halfW, y, halfW, halfH, N, primaryColor, secundaryColor);
            drawHadamard(g, x, y + halfH, halfW, halfH, N, primaryColor, secundaryColor);
            drawHadamard(g, x + halfW, y + halfH, halfW, halfH, N, secundaryColor, primaryColor);
        }
    }

    private void drawRectangle(Graphics2D g, int x, int y, int w, int h, Color primaryColor) {
        g.setColor(primaryColor);
        g.fillRect(x, y, w, h);

        g.setStroke(new BasicStroke(2));
        g.setColor(Color.BLACK);
        g.drawRect(x, y, w, h);
    }
}
