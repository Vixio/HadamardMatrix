package com.tp;

import javax.swing.*;

/**
 * Created by christian on 27/10/15.
 */
public class MainFrame extends JFrame {
    private HadamardPanel hadamardPanel;

    public MainFrame(int N) throws Exception {
        setSize(640, 640);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        hadamardPanel = new HadamardPanel(N);
        add(hadamardPanel);
    }

}
