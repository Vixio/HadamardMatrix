package com.tp;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el valor de N para crear el patron Hadamard: ");
        int N = scanner.nextInt();

        try {
            MainFrame frame = new MainFrame(N);
            frame.setVisible(true);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
